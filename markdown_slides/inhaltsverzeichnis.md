## Inhaltsverzeichnis

1. [Ziele des Trainings](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/lizenzen/html_slides/lizenzen.html#/2)
2. [Urheberrecht und Patenrecht](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/lizenzen/html_slides/lizenzen.html#/3)
3. [Nachnutzungsmöglichkeiten](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/lizenzen/html_slides/lizenzen.html#/5)
4. [(Nach)Nutzungslizenzen](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/lizenzen/html_slides/lizenzen.html#/7)
