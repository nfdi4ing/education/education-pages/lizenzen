## 2. Urheberrecht und Patentrecht

### Urheberrecht

In Deutschland schützt das Urheberrecht grundsätzlich nur die Form, aber nicht den Inhalt. Durch das Urheberrecht werden also keine Fakten und Informationen, wie z.B Messdaten oder Naturgesetze geschützt, da diese nicht vom Menschen geschaffen werden, sondern herausgefunden und entdeckt werden. Wenn die Daten analytisch aufbereitet werden, z.B in Prosaform, dann unterliegt die Darstellungsform, also der Text, dem Urheberrecht, aber nicht die darin enthaltenenen Daten.
<br>
<br>
Das Urheberrecht schützt nur persönliche geistige Schöpfungen, die ein bestimmtes Maß an Originalität aufweisen. Das Ergebnis muss das Resultat einer menschlichen Handlung sein. Die Zuhilfenahme von Maschinen, also inbesondere Computern, beeinflusst dabei nicht, ob das Ergebnis dem Urheberrecht unterliegt. Wenn das Ergebnis aber rein maschinell erzeugt wurde (z.B. mit künstlicher Intelligenz), dann unterliegt es nicht dem Urheberrecht.
Das Gleiche gilt für Methoden und Ideen. Solange sie keine patentierten Erfindungen oder geschützte Gebrauchsmuster sind, unterliegen sie nicht dem Urheberrecht.


#### Urheber oder Rechteinhaber?

Der **Urheber** ist der Schöpfer eines Werkes. Das Urheberrecht ist unübertragbar, es kann nicht an eine andere Person "abgegeben" werden. Urheber können jedoch Nutzungsrechte weitergeben.
Wenn die Daten urheberrechtlich geschützt sind, ist der Urheber auch immer der Rechteinhaber.

Ein **Rechteinhaber** ist eine Person, die die Rechte an einem Werk besitzt. Diese kann sie durch den Erwerb von den Urhebern oder durch andere rechtliche Mittel wie Lizenzverträge erhalten haben. Der Rechteinhaber kann neben dem Urheber auch eine Organisation sein, der die Rechte durch Vereinbarungen übertragen wurden.


#### Wem gehören also die Forschungsdaten?

Sollten Werke im Rahmen eines Auftrags- oder Angestelltenverhältnisses erschaffen werden, entfällt in der Regel allein durch dieses Verhältnis ein Nutzungsrecht an den an den Arbeit- bzw. Auftraggeber.
Das gilt auch für den Hochschulbereich. Werden "Pflichtwerke" von Hochschulangestellten (Assistenten) im Rahmen ihrer Dienstpflichten erstellt, erlangt der Dienstherr/die Hochschule an den erhobenen Forschungsdaten ein Nutzungsrecht.
Sollte ein Wissenschaftler also die Institution wechseln, können die Daten nicht einfach an eine andere Institution mitgenommen werden. Das Gleiche kann im Einzelfall auch für Forschungsdaten gelten, die nicht urheberrechtlich geschützt sind.

<br>
<br>
<br>
<br>
<br>
<br>
<br>

Quellen: https://www.bmbf.de/SharedDocs/Publikationen/de/bmbf/1/31518_Urheberrecht_in_der_Wissenschaft.pdf?__blob=publicationFile&v=6
            <br>
         https://www.forschung-und-lehre.de/forschung/wem-gehoeren-forschungsdaten-1013/ 

<!--### Urheberrecht

**Regeln**
* wissenschaftliche Texte sind in der Regel urheberrechtlich geschützt
* Ausnahmeregelungen (Schrankenregelungen, Fair use)
* Nutzungsrechte werden bei Veröffentlichung meist abgetreten 

**Open Access**
* freier Zugang zu Forschungsergebnissen, insbesondere Publikationen
* ohne finanzielle, technische, rechtliche Barrieren
* Bearbeitung, Weitergabe, Veröffentlichung erlaubt

* Sichtbarkeit erhöhen
* Transparenz herstellen
* Nutzung eigener Werke erleichtern -> **Lizenzen**

-->


### Patentrecht

**Was ist ein Patent?**

*"Patente sind Rechte, mit denen Erfindungen aus allen Gebieten der Technik geschützt werden."* ~ Bundesjustizministerium*

Mit Patenten sollen technische Erfindungen vor Nachahmung geschützt werden. 

Patente müssen beantragt werden und anschließend ein Prüfungsverfahren durchlaufen.
Damit ein Patent erteilt werden kann, müssen folgende **Voraussetzungen** erfüllt sein.

* Es handelt sich um eine neue Erfindung.
* Die Erfindung beruht auf einer erfinderischen Tätigkeit.
* Die Erfindung ist gewerblich anwendbar.

Ein Patent wird für die Dauer von zwanzig Jahren erteilt. 

Als Patentinhaber kann man Lizenzen vergeben. Damit würde die Verwertung im Gegenzug für Lizenzgebühren übertragen werden. 

<br>
<br>
<br>
<br>

*https://www.bmj.de/DE/themen/wirtschaft_finanzen/rechtschutz_urheberrecht/patentrecht/patentrecht_node.html (abgerufen am 21.02.2024)

Weitere Informationen über Patente:
[Deutsches Patent- und Markenamt](https://www.dpma.de/patente/patentschutz/index.html)


### Use-Case

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/lizenzen/-/raw/master/media_files/use_case_patente.png)

Quellen: https://www.wiwo.de/unternehmen/auto/mobilfunk-patente-gerichtsentscheid-verkaufsverbot-fuer-ford-in-deutschland/28363270.html
<br> https://www.heise.de/news/LTE-Patente-Ford-lenkt-ein-und-lizenziert-von-Avanci-7128031.html 