## 1. Ziele des Trainings

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/lizenzen/-/raw/master/media_files/Lernzielmatrix_recht.png)

Quelle: Petersen, Britta, Engelhardt, Claudia, Hörner, Tanja, Jacob, Juliane, Kvetnaya, Tatiana, Mühlichen, Andreas, Schranzhofer, Hermann, Schulz, Sandra, Slowig, Benjamin, Trautwein-Bruns, Ute, Voigt, Anne, & Wiljes, Cord. (2022). Lernzielmatrix zum Themenbereich Forschungsdatenmanagement (FDM) für die Zielgruppen Studierende, PhDs und Data Stewards (Version 1). Zenodo. https://doi.org/10.5281/zenodo.7034478

