## 4. (Nach)Nutzungslizenzen

**Gründe für die Lizenzierung von Forschungsdaten**

* Nachnutzungsmöglichkeiten von Forschungdaten
* Bereitstellung von Informationen darüber, wie Daten/Objekte international genutzt werden dürfen, unabhängig von nationalem Urheberrecht
* Bereitstellung von verlässlichen Informationen zu Nutzungsrechten für andere Forschende


**Creative Commons**

* Lizenzvergabe regelt die Nutzung der Daten durch Dritte und schafft Sicherheit bei der Veröffentlichung und Nutzung publizierter Werke
* CC-Lizenzen: frei verfügbar, standardisiert, einfach zu nutzen

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/lizenzen/-/raw/master/media_files/Lizenzbedingungen.png)


**CC-Lizenzen**

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/lizenzen/-/raw/master/media_files/CC-Lizenzen.png)


### Beispiel: CC-Lizenz für ein Bild vergeben

Wir möchten im Folgenden ein Bild mit einer CC-Lizenz versehen. In diesem Beispiel möchten wir nicht, dass das Bild bearbeitet werden darf. Das heißt, wir entscheiden uns für die **CC BY-ND**-Lizenz.

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/lizenzen/-/raw/master/media_files/ki_generiertes_bild.jpg)
<br>
**"Baum am See"** by **Max Mustermann** is licensed under [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode.en).
<br>
<br>
Diese Zuordnung ist ideal, da sie den Titel ("Baum am See"), den Autor (Max Mustermann) und die Lizenz mit einem Link zum Lizenzvertrag enthält. Alternativ könnte z.B. noch der Titel mit einem Link zum Originalfoto versehen werden, genauso wie der Name des Autors mit einem Link zu seiner Webseite versehen werden könnte.


## Lizenzen

### Open Data Commons

* Offene Lizenz für Daten und/oder Datenbanken
* Open Data Commons Attribution (ODC-By)
    * Namensnennung

* Open Data Commons Public Domain Dedication and License (PDDL)
    * Public Domain / keine Beschränkungen 

* Open Data Commons Open Database License (ODbL)
    * Namensnennung - Weitergabe unter gleichen Bedingungen
    * Bei Verbreitung der Datenbank/Daten oder einer adaptierten Version davon, können (?)

* Weitere Informationen: https://opendatacommons.org/


### Software-Lizenzen

* Software-Code ist automatisch durch das Urheberrecht geschützt. Ohne Lizenz liegen alle Rechte beim Programmierer (Verfasser).
* Eine Software-Lizenz legt die Bedingungen fest, unter denen Software(-Code) genutzt, kopiert, verändert oder verbreitet werden darf.
* Sie kann den Zugang zu Software sowohl erleichtern als auch einschränken

Beispiele
* GPL-3.0 https://choosealicense.com/licenses/gpl-3.0/
* LGPL-3.0 https://choosealicense.com/licenses/lgpl-3.0/
* Apache 2.0 https://choosealicense.com/licenses/apache-2.0/ 
* Public Domain https://choosealicense.com/licenses/unlicense/

<br>
<br>
<br>

Quelle: TU9-FDM. (2019, March). Software als Forschungsdaten (Version 1.0). Zenodo. http://doi.org/10.5281/zenodo.2611303


### Lizenzarten

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/lizenzen/-/raw/master/media_files/Lizenzarten.png)

Quelle: TU9-FDM. (2019, March). Software als Forschungsdaten (Version 1.0). Zenodo. http://doi.org/10.5281/zenodo.2611303


### Auswahl von Lizenzen

Bei der Auswahl von Softwarelizenzen stehen verschiedene Optionen zur Verfügung, die je nach den Zielen und Anforderungen eines Projekts abgewogen werden können.

**Permissive Lizenz** 

Eine Möglichkeit besteht darin, die Verbreitung der Software zu maximieren und gleichzeitig die Einschränkungen für die Nutzung zu minimieren. Dies kann durch die Verwendung einer permissiven Lizenz erreicht werden, die es anderen erlaubt, den Quellcode zu verwenden, zu ändern und zu verbreiten, solange die Lizenzbedingungen eingehalten werden. Ableitungen vom ursprünglichen Quellcode müssen nicht unter der gleichen Lizenz veröffentlicht werden, sondern können auch beispielsweise zu proprietärer Software verarbeitet werden.

**Copy-left Lizenz**

Im Gegensatz nur permissiven Lizenz stellt eine Copy-left Lizenz sicher, dass eine Veränderung des Quellcodes unter derselben Lizenz wie der ursprüngliche Quellcode veröffentlicht wird. Somit wird sichergestellt, dass alle Ableger auch Open-Source bleiben.

**Kombination von verschiedenen Lizenzmodellen**

Eine andere Option ist die Kombination von Lizenzmodellen. 
Man kann z.B. eine Open-Source Lizenz für die akademische Nutzung vergeben und eine proprietäre für die Nutzung in der Industrie.

**Proprietäre Lizenz**

Natürlich besteht auch die Möglichkeit, die Software unter eine propritären Lizenz zu veröffentlichen. Diese Möglichkeit gewährleistet eine maximale Kontrolle über die Verwendung des Quellcodes.
<!-- 
* Ausbreitung maximieren, Restriktionen minimieren
    * Permissive Lizenz

* Sicherstellen, dass alle Ableger auch Open Source sind
    * Copy-left Lizenz

* Open Source für die akademische Welt, Geld verdienen an Wirtschaft
    * Open-Source-Lizenz + kostenpflichtger Support-Vertrag
    * Mehrere Lizenzen anbieten (z.B. Open-Source für akedemische Nutzung und proprietäre), siehe z.B. MySQL

* Quellcode schützen, maximale Kontrolle
    * Proprietäre Lizenz
-->

*Morin A, Urban J, Sliz P (2012). A Quick Guide to Software Licensing for the Scientist-Programmer. PLoS Comput Biol 8(7): e1002598. https://doi.org/10.1371/journal.pcbi.1002598* <br>
Quelle: TU9-FDM. (2019, March). Software als Forschungsdaten (Version 1.0). Zenodo. http://doi.org/10.5281/zenodo.2611303


**Lizenzvergabe-Tools**

* Choose A License: Entscheidungshilfe bei der Wahl einer Lizenz für die eigene Software
https://choosealicense.com/ 

* Fossology: Open-Source Toolbox, Framework und Web-Applikation zum Extrahieren von Lizenzinformationen aus Quellcode
https://www.fossology.org/ 

* Fossa: Kommerzielles Tool zum automatischen Check der Lizenzkompatibilität, integrierbar in den Entwicklungsprozess
https://fossa.com/ 

Lizenzvergabe
* Closed source: durch End User License Agreement (EULA), das dem Endbenutzer vor Vertragsabschluss zur Verfügung gestellt werden muss
* Open source: Verweis auf Lizenz in jedem Quelltextdokument
LICENSE, COPYING, license.txt

Quelle: TU9-FDM. (2019, March). Software als Forschungsdaten (Version 1.0). Zenodo. http://doi.org/10.5281/zenodo.2611303

* Standardisierte Methode zur Angabe von Urheberrechten und Lizenzen für Softwareprojekte:
https://reuse.software/spec/