## 3. Nachnutzungsmöglichkeiten

**Eine Lizenz kann Nachnutzungsmöglichkeiten erleichtern**

* Wie ein Datensatz nachgenutzt werden darf, wird meistens über Lizenzen verbindlich geregelt. Viele Repositorien vergeben Lizenzen der Creative Commons Familie <U>[CC-Lizenzen](https://de.creativecommons.net/start/)</U>
* Neben CC-Lizenzen gibt es für Softwareforschungsdaten eigene Lizenzen (MIT, GNU oder APACHE oder auch die ODC (Open Data Commons) Lizenzen)

* Falls keine Lizenz vergeben wurde, sollten Sie:
    * Den Urheber kontaktieren und die Nutzungsrechte einholen!

* Verwendung mehrerer Datensätze in der Forschung:
* Beim Nachnutzen mehrerer Datensätze mit unterschiedlichen Lizenzen müssen die diesen Datensätzen zugewiesenen Lizenzen miteinander kompatibel sein
    * <U>Kompatibilitätstabelle</U>

*[Weitere Lerninhalte sind in Phase 6 des Datenlebenslebenszyklus zu finden, die das Thema "Forschungsdaten nachnutzen" behandelt.](https://nfdi4ing.pages.rwth-aachen.de/education/education-pages/dlc-datalifecycle/html_slides/dlc6.html#/) 


**Vereinfachtes Entscheidungsschaubild für die Nutzung fremder Daten**

![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/lizenzen/-/raw/master/media_files/Entscheidungsschaubild.png)

<br>
<br>

Quelle: Daudrich et al. (2019) / CC-BY 3.0 
